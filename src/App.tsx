import * as React from 'react'
import { useEffect, useState, useCallback } from 'react'
import Clock from './Clock'
import SignIn from './SignIn'

const apiRequest = (params: any) => fetch(params).then((response) => response.json())
const apiGetUsers = () => apiRequest('https://jsonplaceholder.typicode.com/users')
const apiGetPosts = (userId: string) => apiRequest(`https://jsonplaceholder.typicode.com/posts?userId=${userId}`)
const apiGetComments = (postId: string) => apiRequest(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`)

const DEFAULT_PARENT_ID = 'DEFAULT_PARENT_ID'

type ComponentProps = {
  parentId?: string,
  entityRenderer: (arg: any) => JSX.Element,
  onSelect?: (id: string) => void,
  fetchData: () => Promise<[]>
}

const EntityList = ({ parentId = DEFAULT_PARENT_ID, entityRenderer, onSelect, fetchData }: ComponentProps): JSX.Element => {
  const [entities, setEntities] = useState([])
  const [isPending, setIsPending] = useState(false)

  const loadEntities = useCallback(() => {
    if (!fetchData) {
      return
    }

    setIsPending(true)

    fetchData().then((response) => {
      setEntities(response)
      setIsPending(false)
    }
    )
  }, [fetchData])


  useEffect(() => {
    setEntities([])
    loadEntities()
  }, [parentId, loadEntities, fetchData])


  if (!entities.length) {
    return null
  }

  if (isPending) {
    return <p>'Loading...'</p>
  }

  return (
    <div>
      {entities.map((entity) => (
        <div
          key={entity.id}
          onClick={onSelect ? () => onSelect(entity.id) : undefined}
        >
          {entityRenderer(entity)}
        </div>
      ))}
    </div>
  )
}

const App = () => {
  const [selectedUserId, setSelectedUserId] = useState(null)
  const [selectedPostId, setSelectedPostId] = useState(null)

  const renderUser = (user: any) => (
    <div
      className={
        'item' + (user.id === selectedUserId ? ' active' : '')
      }
    >
      {user.name}
    </div>
  )

  const renderPost = (post: any) => (
    <div
      className={
        'item' + (post.id === selectedPostId ? ' active' : '')
      }
    >
      {post.title}
    </div>
  )

  const renderComment = (comment: any) => <div className="active">{comment.name}</div>

  const handleUserSelect = (userId: string) => {
    setSelectedPostId(null)
    setSelectedUserId(userId)
  }

  const handlePostSelect = (postId: string) => setSelectedPostId(postId)

  return (
    <div className="wrapper">
    <div className="entities">
      <div className="entity-block">
        <h2>Users</h2>
        <EntityList
          onSelect={handleUserSelect}
          fetchData={apiGetUsers}
          entityRenderer={renderUser}
        />
      </div>
      <div className="entity-block">
        <h2>Posts</h2>
        <EntityList
          onSelect={handlePostSelect}
          parentId={selectedUserId}
          fetchData={
            selectedUserId ? () => apiGetPosts(selectedUserId) : undefined
          }
          entityRenderer={renderPost}
        />
      </div>
      <div className="entity-block">
        <h2>Comments</h2>
        <EntityList
          parentId={selectedPostId}
          fetchData={
            selectedPostId ? () => apiGetComments(selectedPostId) : undefined
          }
          entityRenderer={renderComment}
        />
      </div>
    </div>
      <br/>
      <Clock/>
      <br/>
      <SignIn/>
    </div>
  )
}

export default App;
