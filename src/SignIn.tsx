import * as React from 'react'

const apiRequest = (params: RequestInfo) =>
  fetch(params).then(
    (response) =>
      Math.random() < 0.5 ? new Error("Unexpected API error") : response.json() // We want to simulate unstable API
  );
const apiGetUsers = () =>
  apiRequest("https://jsonplaceholder.typicode.com/users");

const UserProfile = ({ userId }: {userId: any}) => {
  const [loggedUserProfile, setLoggedUserProfile] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  const [isError, setIsError] = React.useState(false);
  
  const loadData = React.useCallback(() => {
    setLoading(true);

    apiGetUsers().then((response) =>
      { 
        if (!response.length) {
          throw new Error(response)
        }

        const loggedUser = response.find((user: { id: any; }) => user.id === Number(userId))
        
        setLoggedUserProfile(loggedUser);
        setIsError(false);
        setLoading(false);
        }, 
      ).catch((error) => 
      {
        alert(error)
        setIsError(true);
        setLoading(false);
      })
  }, [userId]);

  React.useEffect(() => {
    userId ? loadData() : setLoggedUserProfile(null);
  }, [loadData, userId]);


  if (loading) {
    return <div>Loading...</div>;
  }

  if (isError) {
    return <div>Error!</div>
  }

  if (!loggedUserProfile) {
    return <div>User not found!</div>
  }

  return <div>Name: {loggedUserProfile.name}</div>;
};

const SignIn = ({ onSignIn, onSignOut }: {onSignIn: any, onSignOut: any}) => {
  const [userId, setUserId] = React.useState();
  const [isSignedIn, setSignedIn] = React.useState(false);

  React.useEffect(() => {
    setSignedIn(false);
  }, [userId]);

  const onUserChange = (event: any) => {
    setUserId(event.target.value)
    onSignOut();
  };

  const onSignInClick = () => {
    setSignedIn(true);
    onSignIn(userId);
  };

  return (
    <>
      <div>{isSignedIn ? `Signed In as ${userId}!` : "Signed Out!"}</div>
      <div>
        <select onChange={onUserChange}>
          <option disabled selected>
            Select user
          </option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>666</option>
        </select>
        <button onClick={onSignInClick} disabled={Boolean(!userId)}>
          Sign In
        </button>
      </div>
    </>
  );
};

const App = () => {
    const [userId, setUserId] = React.useState();
  
    const onSignOut = () => setUserId(undefined);
  
    return (
      <>
        <SignIn onSignOut={onSignOut} onSignIn={setUserId} />
        <UserProfile userId={userId} />
      </>
    );
  };

export default App