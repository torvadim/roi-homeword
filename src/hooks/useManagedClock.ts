import { useEffect, useState } from "react";

const useManagedClock = (
    initialDate: Date = new Date(),
    initialTimezoneOffset: number = new Date().getTimezoneOffset() / 60
  ): {
    date: Date;
    timezoneOffset: number;
    shiftForward: () => void;
    shiftBackward: () => void;
  } => {
    const [date, setDate] = useState(initialDate)
    const [timezoneOffset, setTimezoneOffset] = useState(initialTimezoneOffset)

    
    useEffect(() => {
        if (Math.abs(date.getDay() - initialDate.getDay()) > 1) {
            setDate(initialDate)
            setTimezoneOffset(initialTimezoneOffset)

        }

        if (timezoneOffset > 12) {
            setTimezoneOffset(-11)
        }

        if (timezoneOffset < -11) {
            setTimezoneOffset(12)
        }

    }, [date, initialDate, initialTimezoneOffset, timezoneOffset])
    
    
    const shiftForward = () => {
        setTimezoneOffset(prev => prev + 1)
        setDate(new Date(date.setHours(date.getHours() + 1)))
    }

    const shiftBackward = () => {
        setTimezoneOffset(prev => prev - 1)
        setDate(new Date(date.setHours(date.getHours() -1)))
    }
  
    return {
      date,
      timezoneOffset,
      shiftForward,
      shiftBackward
    };
  };

  export default useManagedClock